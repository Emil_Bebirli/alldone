import java.util.Scanner;

public class CycleThird  {
    public static void main(String[] args) {
        System.out.print("Введите размер массива: ");
        Scanner scanJ = new Scanner(System.in);
        int v = scanJ.nextInt();
        double e = 0;
        double[] myArray = new double[v];
        for (int i = 0; i <= myArray.length - 1; i++) {
            System.out.print("Массив[" + i + "] = ");
            Scanner scanArray = new Scanner(System.in);
            myArray[i] = scanArray.nextDouble();
            e += myArray[i];
        }
        e /= v;
        for (int i = 0; i <= myArray.length - 1; i++) {
            System.out.print(myArray[i] * e + " ");
        }
    }
}
