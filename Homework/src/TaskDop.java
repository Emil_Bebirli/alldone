import java.util.Scanner;
public class TaskDop {
    public static void main(String[] args) {
        byte n = 0;
        do {
            System.out.print("Введите размер массива: ");
            Scanner scan = new Scanner(System.in);
            if (scan.hasNextByte()) {
                n = scan.nextByte();
                if (n <= 0) {
                    System.out.println("Некорректный ввод");
                }else{
                    break;
                }
            } else {
                System.out.println("Некорректный ввод");
            }
        }
        while (n <= 0);
        double[] myArray = new double[n];
        Scanner scan2 = new Scanner(System.in);
        for (int i = 0; i <= myArray.length - 1; i++) {
            System.out.print("Массив[" + i + "] = ");
            myArray[i] = scan2.nextDouble();
        }
        double max, min;
        int iMax = 0, iMin = 0;
        min = max = myArray[0];
        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] > max) {
                max = myArray[i];
                iMax = i;
            } else if (myArray[i] < min) {
                min = myArray[i];
                iMin = i;
            }
        }
        myArray[iMax] = min;
        myArray[iMin] = max;
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("Массив[" + i + "] = " + myArray[i]);
        }
        if (max - min < 2) {
            System.out.println("Разность макс и мин не превосходит двух");
        } else {
            System.out.println("Разность макс и мин больше двух");
        }

    }
}



