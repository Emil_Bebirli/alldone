import java.util.Scanner;

public class DataWorkFirst {
    public static void main(String[] args) {
        System.out.print("Введите первое число: ");
        Scanner scan = new Scanner(System.in);
        Integer t = scan.nextInt();
        String j = Integer.toString(t);
        System.out.print("Введите второе число: ");
        int v = scan.nextInt();
        int result = t.compareTo(v);
        if (result == 1) {
            System.out.println("Большее число: " + t);
            System.out.print("Меньшее число: " + (double) v);
        } else if (result == 0) {
            System.out.println("Числа равны");
        } else {
            System.out.println("Большее число: " + v);
            System.out.print("Меньшее число: " + t.doubleValue());
        }
    }
}
